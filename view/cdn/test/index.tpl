{if $this->isAllowed('ApiDocs', 'access')}
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Wintersporters.nl Test suite</a>
        </div>
      </div>
    </div>
    
    <script>
    
    	function setRepsonseStatus(id, ok) {
    		$('#' + id).find('span.status-icon ').removeClass('glyphicon-question-sign').removeClass('bg-info');
    		
    		if(ok) {
    			$('#' + id).find('span.status-icon ').addClass('bg-success').addClass('glyphicon-ok-sign');
    		} else {
    			$('#' + id).find('span.status-icon ').addClass('bg-danger').addClass('glyphicon-remove-sign');
    		}
    	}
    </script>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">

			{$this->partial('api/docs/status.tpl', ['test' => true])}
			
			{$this->partial('api/docs/destinations.tpl', ['test' => true])}
			
			{$this->partial('api/docs/destinations-countries.tpl', ['test' => true])}
			
			{$this->partial('api/docs/destinations-country-data.tpl', ['test' => true])}
			
			{$this->partial('api/docs/destinations-info.tpl', ['test' => true])}
			
			{$this->partial('api/docs/destinations-list.tpl', ['test' => true])}
			
			{$this->partial('api/docs/destinations-regions.tpl', ['test' => true])}
			
			{$this->partial('api/docs/destinations-sync.tpl', ['test' => true])}
			
			{$this->partial('api/docs/destinations-locations.tpl', ['test' => true])}
			
			{$this->partial('api/docs/snowforecast-maps.tpl', ['test' => true])}
			
			{$this->partial('api/docs/snowforecast-thumbs.tpl', ['test' => true])}
						
			{$this->partial('api/docs/snowalerts-register.tpl', ['test' => true])}
			
			{$this->partial('api/docs/updates-submit.tpl', ['test' => true])}
			
			{$this->partial('api/docs/updates-comments.tpl', ['test' => true])}
			
			{$this->partial('api/docs/updates-destinations.tpl', ['test' => true])}
			
			{$this->partial('api/docs/updates-like.tpl', ['test' => true])}
			
			{$this->partial('api/docs/updates-report.tpl', ['test' => true])}

			{$this->partial('api/docs/snow-snowreport.tpl', ['test' => true])}
			
			{$this->partial('api/docs/weather.tpl', ['test' => true])}
			
			{$this->partial('api/docs/webcams.tpl', ['test' => true])}
			
			{$this->partial('api/docs/webcams-near.tpl', ['test' => true])}
			
			{$this->partial('api/docs/webcams-list.tpl', ['test' => true])}
		
        </div>
      </div>
    </div>
{else}
	<p>You're not allowed to view these docs.</p>
{/if}