<!DOCTYPE html>					
<html>
<head>
{$this->headMeta()->setCharset('utf-8')
					  ->appendName('viewport', 'width=device-width, height=device-height')
					  ->appendName('robots', 'noindex, nofollow')}
<title>wePowder CDN documentation</title>
</head>
<body>
	{$this->content}
</body>
</html>