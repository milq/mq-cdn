<?php  
namespace Cdn\View\Helper;  

use Zend\View\Helper\AbstractHelper;  
use Zend\ServiceManager\ServiceLocatorAwareInterface;  
use Zend\ServiceManager\ServiceLocatorInterface;  

class Cdn extends AbstractHelper implements ServiceLocatorAwareInterface   
{    
    private $_basePath;
    private $_staticPath;
    
    private $serviceLocator;
    
    public function __invoke($imgPath, $imgId = null, $params = array(), $dirNo = null, $basePath = null)  
    {      	
        if($basePath == null)
        	$this->_getBasePath();
        else
        	$this->_basePath = $basePath;
        
        if(!isset($imgPath))
        	return null;
        	
        if($imgId === null)
        	return $this->_staticPath . $imgPath;

        $_dirNo = ($dirNo == null) ? ceil($imgId / 20000) : $dirNo; 
        $params = $this->_mergeParams($params);        

        return $this->_basePath . $imgPath . '/' . $_dirNo . '/' . $imgId . $params . '.jpg';
    }
    
    private function _getBasePath() {
	    
	    if($this->_basePath != null)
	    	return $this->_basePath;
	    
	    $sm = $this->getServiceLocator()->getServiceLocator();  
		$config = $sm->get('application')->getConfig();  

	    $urlHelper = $this->view->plugin('url');
	    
	    $this->_basePath = $urlHelper('cdn', array('domain' => $config['cdn']['domain'] ));
	    $this->_staticPath = $urlHelper('cdn', array('domain' => $config['cdn']['domain'], 'subdomain' => 'static' ));

	    return $this->_basePath;
	}
	
	private function _mergeParams($params) {
		
		if(count($params) == 0)
			return '';

		return '-' . implode('-', $params);
	}
	
	/** 
     * Set the service locator. 
     * 
     * @param ServiceLocatorInterface $serviceLocator 
     * @return CustomHelper 
     */  
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)  
    {  
        $this->serviceLocator = $serviceLocator;  
        return $this;  
    }  

    /** 
     * Get the service locator. 
     * 
     * @return \Zend\ServiceManager\ServiceLocatorInterface 
     */  
    public function getServiceLocator()  
    {  
        return $this->serviceLocator;  
    } 
}  