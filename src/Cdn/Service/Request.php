<?php

namespace Cdn\Service;

class Request
{
	private $controller;
	private $dir;
	private $filename;
	private $filenameWithParams;
	private $params = [];
	
	public function __construct($controller, $dir, $filename, $paramString, $namespace = null) {
		
		$this->controller = ($namespace) ? $namespace . '/' . $controller : $controller;
		$this->dir = $dir;
		$this->filename = $filename;
		
		$params = explode('-', $paramString);
        
		$this->params = $params;
		$this->filenameWithParams = $filename . '-' . $paramString;
		
		return $this;
    }
    
    public function getParam($key) {
	    
	    if(!isset($this->{$key}))
	    	return null;
	    	
	    return $this->{$key};
    }
}