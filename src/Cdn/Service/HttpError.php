<?php

namespace Cdn\Service;

class HttpError
{
	public function notFound($width = 48, $height = 48) {
		
		if($height === false || $height === null)
			$height = 48;
			
		$im = imagecreatetruecolor($width, $height);
		imagefill($im, 0, 0, imagecolorallocate($im, 220,146,138));
		
		ob_clean();
		
		header('Content-Type: image/jpeg', true);
		
		imagejpeg($im);
		imagedestroy($im);
		
		exit;
    }
    
    public function renderedButNotFound($file) {
	    
	    header('Content-Type: text/plain', true);
	    
	    die('This image is already rendered, where is it? (' . $file . ')');
    }
}