<?php

namespace Cdn\Service;

class Image
{
	const CDN_DEFAULT_WIDTH = 320;
    const CDN_DEFAULT_QUALITY = 100;
    const CDN_DEFAULT_RESAMPLE_QUALITY = 5;

    private $_request;
    private $_filepathOriginal;
    private $_filepathRendered;
    private $_originalDir;
    private $_renderedDir;
    private $_forceWidth = false;
    private $_error = false;
    private $_errorMessage;
    
    private $_originalImageData;
    private $_originalImageWidth;
    private $_originalImageHeight;
    
    private $_newImageData;
    private $_newImageWidth;
    private $_newImageHeight;
    private $_newImageQuality = self::CDN_DEFAULT_QUALITY;
    private $_cropFeature = false;
    private $_rotateFeature = false;
    private $_posX;
    private $_posY;
    
    private $_imageResampleQuality = self::CDN_DEFAULT_RESAMPLE_QUALITY;
    private $_imageWith = self::CDN_DEFAULT_WIDTH;
    private $_imageHeight = false;
    private $_allowedImageSizes = array(0, 48, 64, 80, 160, 320, 640, 800, 1024, 1280, 2560);
    private $_allowedCrops = array('v','h','c');

	public static function factory($request = null, $originalDir = 'assets/originals', $renderedDir = 'assets/rendered')
    {
    	return new self($request, $originalDir, $renderedDir);
    }
    
    public function __construct($request, $originalDir, $renderedDir) {
		
		if($request !== null)
		    $this->setRequest($request);
		    
		$this->_originalDir = $originalDir;
		$this->_renderedDir = $renderedDir;
    }
    
    public function createResizedImage() 
    {
    	$this->__parseParams();

    	if(!$this->_error) 
        {
            if(!$this->__getImage($this->getOriginalFilePath()))
                return false;
				
            $this->__resizeImage($this->_imageWith, $this->_imageHeight, $this->_cropFeature);

            if($this->_cropFeature)
                $this->__cropImage($this->_imageWith, $this->_imageHeight, $this->_cropFeature);
                  
            $output = $this->__outputImage($this->getRenderedFilePath(), $this->_newImageData, $this->_newImageQuality);
            
            return $output;
        }

        return false;
    }
    
    public function getParams() {
	    
	    $this->__parseParams();
	    
	    return array('h' => $this->_imageHeight, 'w' => $this->_imageWith);
    }
    
    public function getLastModifiedTimestamp() {
	    
	    return filemtime($this->getOriginalFilePath());
    }
    
    public function getEtag() {
	    
	    return md5_file($this->getOriginalFilePath());
    }
    
    public function rotateImage() 
    {
    	$this->__parseParams();

    	if(!$this->_error) 
        {
            if(!$this->__getImage($this->getOriginalFilePath()))
                return false;
             
            // Resample the image.
            $tempImg = imagecreatetruecolor($this->_originalImageWidth, $this->_originalImageHeight);
            imagecopyresampled($tempImg, $this->_originalImageData, 0, 0, 0, 0, $this->_originalImageWidth, $this->_originalImageHeight, $this->_originalImageWidth, $this->_originalImageHeight);

            $degrees = ($this->_rotateFeature == 'l') ? 90 : -90;
            
            // Rotate the image.
            $rotatedImage = imagerotate($this->_originalImageData, $degrees, 0);

            if(!imagejpeg( $rotatedImage, $this->getOriginalFilePath(), $this->_newImageQuality))
            	return false;
            	
            $this->__removeCachedImages();
            $this->__imageDestroy();

            return true;
        }

        return false;
    }
    
    public function cutoutImage() {
	    
	    $this->__parseParams(true);

    	if(!$this->_error) 
        { 
            if(!$this->__getImage($this->getOriginalFilePath()))
                return false;
			
			$this->__resizeImage(800, 500, null);
								
			// Resample the image.
            $newImg = imagecreatetruecolor($this->_imageWith, $this->_imageHeight);
			imagecopyresampled($newImg, $this->_newImageData, 0, 0, $this->_posX, $this->_posY, $this->_newImageWidth, $this->_newImageHeight, $this->_newImageWidth, $this->_newImageHeight);
			
			$this->__removeCachedImages();
            $this->__imageDestroy();

            return $this->__outputImage($this->getRenderedFilePath(), $newImg, $this->_newImageQuality);
        }

        return false;
    }

    public function moveImage($originalFilePath, $newFilePath) 
    {
        if(!$this->__getImage($originalFilePath))
            return false;
        
        // Check exif orientation and correct the rotation if needed. Is needed for iPhones etc.
        $this->correctOrientation($originalFilePath);
  
        $result = $this->__outputImage($newFilePath, $this->_originalImageData, 100);
		
		@chmod($newFilePath, 0775);
		
        unlink($originalFilePath);

        return $result;
    }
    
    public function correctOrientation($path) {
	    
	    $exif = @exif_read_data($path);
		
		if(!isset($exif['Orientation']))
			return false;
			
		$ort = $exif['Orientation'];

	    switch($ort)
	    {
	        case 1: // nothing
	        break;                               
	        case 3: // 180 rotate left
	            $this->__rotateOriginal(180);
	        break;	                
	        case 6: // 90 rotate right
	            $this->__rotateOriginal($public, -90);
	        break;	                
	        case 8:    // 90 rotate left
	            $this->__rotateOriginal($public, 90);
	        break;
	    }
    }
    
    public function copyImage($request) 
    {
    	$source = $this->_originalDir . '/' . $request->sourcecontroller . '/' . $request->sourcedir . '/' . $request->sourcefilename . '.jpg';
    	$destination = $this->_renderedDir . '/' .  $request->destcontroller . '/' . $request->destdir . '/' . $request->destfilename . '.jpg';

    	if(!file_exists($source))
    		return false;
    		
		copy($source, $destination);
		
		foreach( glob($this->_renderedDir . '/' . $request->destcontroller . '/'. $request->destdir . '/' . $request->destfilename . '*' ) as $file )
		{
        	unlink($file);
        }
		
		return true;
    }
 
    public function originalExists() 
    {
    	return file_exists($this->getOriginalFilePath());
    }

    public function isRendered() 
    {
    	return file_exists($this->getRenderedFilePath());
    }

    public function getRenderedFilePath() 
    {
    	if(!$this->_filepathRendered)
    		$this->__setFilePathRendered();
    		
        return $this->_filepathRendered;
    }
    
    public function getOriginalFilePath() 
    {
    	if(!$this->_filepathOriginal)
    		$this->__setFilePathOriginal();
    		
        return $this->_filepathOriginal;
    }
    
    private function __setFilePathRendered()
    {
        $filename = (substr($this->getRequest('filenameWithParams'), -1) == '-') ? substr($this->getRequest('filenameWithParams'), 0, -1) : $this->getRequest('filenameWithParams');
	    $this->_filepathRendered = $this->_renderedDir . '/' . $this->getRequest('controller') . '/' . $this->getRequest('dir') . '/' . $filename . '.jpg';
    }
    
    private function __setFilePathOriginal()
    {
	    $this->_filepathOriginal = $this->_originalDir . '/' . $this->getRequest('controller') . '/' . $this->getRequest('dir') . '/' . $this->getRequest('filename') . '.jpg';
    }
    
    public function  __removeCachedImages()
	{
		foreach( glob($this->_renderedDir . '/' . $this->getRequest('controller') . '/'. $this->getRequest('dir') . '/' . $this->getRequest('filename') . '*' ) as $file )
		{
        	unlink($file);
        }
	}
    
    private function __getImage($filePath) 
    {
        $size = @getimagesize(str_replace('.jpg','.jpg',$filePath));
       
        switch($size['mime']) {
            case 'image/gif':
                $img = ImageCreateFromGif($filePath);
                break;
            case 'image/jpg':
            case 'image/jpeg':
                $img = ImageCreateFromJpeg($filePath);
                break;
            case 'image/png':
                $img = ImageCreateFromPng($filePath);
                break;
            default:
                $img = false;
        }

        if(!$img)
            return false;

        $width = imagesx( $img );
        $height = imagesy( $img );

        $this->_originalImageData = $img;
        $this->_originalImageWidth = $width;
        $this->_originalImageHeight = $height;

        return true;
    }

    private function __resizeImage($width, $height, $crop) 
    {
    	if($width > 0) {
	        if($crop == 'c' && $height) {
	
	        	if($this->_originalImageWidth > $this->_originalImageHeight) {
		        	
		        	$ratio = $height / $this->_originalImageHeight;
		        	$width = $this->_originalImageWidth * $ratio;
		        	$this->_cropFeature = 'h';
		        	
	        	} else {
		        	
		        	$ratio = $width / $this->_originalImageWidth;
		        	$height = $this->_originalImageHeight * $ratio;
		        	$this->_cropFeature = 'v';
	        	}
	        	
	        } else {
	        	
	        	$ratio = $width / $this->_originalImageWidth;
	        	$height = $this->_originalImageHeight * $ratio;
	        }
	
	        $newImage = imagecreatetruecolor( $width, $height );
	        
	        imagecopyresampled($newImage, $this->_originalImageData, 0, 0, 0, 0, $width, $height, $this->_originalImageWidth, $this->_originalImageHeight);
	    
	    } else {
		    
		 	$newImage = $this->_originalImageData;   
	    }	    

        $this->_newImageData = $newImage;
        $this->_newImageWidth = round($width);
        $this->_newImageHeight = round($height);
    }

    private function __cropImage($width, $height, $crop) 
    {    
        if($crop == 'v') {

            $srcX = 0;
            $srcY = round(($this->_newImageHeight - $height) / 2);

            $newImage = imagecreatetruecolor( $width, $height );
    
            imagecopyresampled($newImage, $this->_newImageData, 0, 0, $srcX, $srcY, $width, $height, $width, $height);

            $this->_newImageData = $newImage;
            
        } else if($crop == 'h') {

            $srcX = round(($this->_newImageWidth - $width) / 2);
            $srcY = 0;

            $newImage = imagecreatetruecolor( $width, $height );
    
            imagecopyresampled($newImage, $this->_newImageData, 0, 0, $srcX, $srcY, $width, $height, $width, $height);

            $this->_newImageData = $newImage;
        }
    }
    
    private function __rotateOriginal($degrees)
    {
    	// Rotate the image.
        $rotatedImage = imagerotate($this->_originalImageData, $degrees, 0);

        $this->_originalImageData = $rotatedImage;
    }

    private function __outputImage($renderedFilePath, $imageData, $quality) {
	
        $dir = dirname($renderedFilePath);

        if(!is_dir($dir))
        {
            mkdir($dir,0755,true);
        }
  
        if(!imagejpeg( $imageData, $renderedFilePath, $quality))
            return false;
         
        ob_start();    
        @system('jpegoptim -p -m75 --strip-all ' . $renderedFilePath);
        ob_clean();
        
        @chmod($renderedFilePath, 0664);
        
        ob_start();
        
        imageinterlace($imageData, true);
        imagejpeg($imageData);
        
        $output = ob_get_clean();
        
        $this->__imageDestroy();

        return $output;
    }
    
    private function __imageDestroy()
    {
	    // release the memory
        if(is_resource($this->_newImageData))
            imagedestroy($this->_newImageData);
        if(is_resource($this->_originalImageData))
            imagedestroy($this->_originalImageData);

    }

    private function __parseParams($allowAll = false) {
		
		$params = $this->getRequest('params');
		
    	foreach ($params as $value) {
            
            $param = substr($value, 0, 1);
            $paramValue = substr($value, 1);

            switch ($param) {
                // Image width
                case 'w':
                    if(!in_array($paramValue, $this->_allowedImageSizes) && $allowAll === false)
                        $this->__error('Invalid width');

                    $this->_imageWith = $paramValue;
                    break;
                // Image height
                case 'h':
                    if(!in_array($paramValue, $this->_allowedImageSizes) && $allowAll === false)
                        $this->__error('Invalid height');

                    $this->_imageHeight = $paramValue;
                    break;
                // Crop feature
                case 'c':
                    if(!in_array($paramValue, $this->_allowedCrops))
                        $this->__error('Invalid crop feature');

                    $this->_cropFeature = $paramValue;
                    break;
                // Rotate feature
                case 'r':
                    if(!in_array($paramValue, array('l','r')))
                        $this->__error('Invalid rotate feature');

                    $this->_rotateFeature = $paramValue;
                    break;
                case 'x':
                    $this->_posX = $paramValue;
                    break;
                case 'y':
                    $this->_posY = $paramValue;
                    break;
            }
        }
    }

    private function __error($error) {

        $this->_error = true;
        $this->_errorMessage = $error;
    }
    
    private function setRequest(Request $request) {
	    
	    $this->_request = $request;
    }
    
    private function getRequest($key = null) {
	    
	    if($key === null)
		    return $this->_request;
	
		return $this->_request->getParam($key);
    }
}