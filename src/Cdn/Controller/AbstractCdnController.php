<?php

namespace Cdn\Controller;

use Zend\Mvc\Controller\AbstractActionController as ActionController;
use Zend\ServiceManager\ServiceLocatorInterface;

class AbstractCdnController extends ActionController
{	
	protected $serviceLocator;
		
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
}