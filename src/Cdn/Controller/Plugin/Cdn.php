<?php

namespace Cdn\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Cdn extends AbstractPlugin implements ServiceLocatorAwareInterface  
{
	private $serviceLocator;
	
    public function __invoke($imgPath, $imgId = null, $params = array(), $dirNo = null) {
		
		$sl = $this->getServiceLocator()->getServiceLocator();
		$cdnHelper = $sl->get('viewhelpermanager')->get('cdn');
		
		$url = $cdnHelper($imgPath, $imgId, $params, $dirNo);	
		
		return $url;
	}
	
	/** 
     * Set the service locator. 
     * 
     * @param ServiceLocatorInterface $serviceLocator 
     * @return CustomHelper 
     */  
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)  
    {  
        $this->serviceLocator = $serviceLocator;  
        return $this;  
    }  

    /** 
     * Get the service locator. 
     * 
     * @return \Zend\ServiceManager\ServiceLocatorInterface 
     */  
    public function getServiceLocator()  
    {  
        return $this->serviceLocator;  
    } 
}