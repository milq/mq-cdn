<?php
namespace Cdn\Controller;

use Cdn\Service\Image;
use Cdn\Service\Request;
use Cdn\Service\HttpError;

class ImageController extends AbstractCdnController
{	
	public function indexAction() {

		$config = $this->getServiceLocator()->get('Config');
		
		$params = $this->params();
		$request = new Request($params->fromRoute('imageType'), $params->fromRoute('dir'), $params->fromRoute('imageId'), $params->fromRoute('params'), $params->fromRoute('namespace'));

		$image = Image::factory($request, $config['cdn']['originalsDirectory'], $config['cdn']['renderedDirectory']);
		$params = $image->getParams();
	
		$error = new HttpError();
		
		if(!$image->originalExists()) {
					
			$error->notFound($params['w'], $params['h']);
		}

		if($image->isRendered() && !$image->originalExists())
			$error->renderedButNotFound($image->getRenderedFilePath());
		
		$imageBin = $image->createResizedImage();

		if($imageBin) {
			
			ob_clean();
			
			header('Content-Type:image/jpeg');
			die($imageBin);
			
		} 
		
		$error->notFound($params['w'], $params['h']);
		exit;
	}
}