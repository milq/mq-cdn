<?php
namespace Cdn\Controller;

use Cdn\Service\Request;
use Cdn\Service\Image;
use Cdn\Exception\RuntimeException;
 
class UploadController extends AbstractCdnController
{	
	public function indexAction() {
	
		if(!$this->params()->fromPost('imageType', $this->params()->fromQuery('imageType')))
			die('imageType is missing');
		
		ini_set('default_charset', 'UTF-8');
		putenv('LC_TIME=nl_NL');
		setlocale(LC_TIME, 'nl_NL.UTF-8');
		
		header("Content-type: text/html; charset=UTF-8",true);
		header("Access-Control-Allow-Headers: x-requested-with,x-file-name,x-mime-type,content-type");
		header("Access-Control-Allow-Methods: POST");
		
		$isAdminRequest = ($this->params()->fromRoute('source', 'user') == 'admin');
		$imageType = $this->params()->fromPost('imageType', $this->params()->fromQuery('imageType'));
		$contentId = $this->params()->fromPost('contentId', $this->params()->fromQuery('contentId'));
		$userId = $this->params()->fromPost('userId', $this->params()->fromQuery('userId'));
		
		if($_SERVER['REQUEST_METHOD'] === 'OPTIONS')
			return true;
		
		$config = $this->getServiceLocator()->get('Config');

		try
		{	
			if(!isset($config['cdn']['upload_strategies'][$imageType]))
				throw new RuntimeException('No upload strategy found for the image type: ' . $imageType);
			
			if(!isset($config['cdn']['upload_paths'][$imageType]))
				throw new RuntimeException('No upload path found for the image type: ' . $imageType);
				
			if(!isset($config['cdn']['public_paths'][$imageType]))
				throw new RuntimeException('No public path found for the image type: ' . $imageType);
				
			$uploader = new \Cdn\Service\Fileupload($config['cdn']['allowedExtensions'], ($isAdminRequest) ? $config['cdn']['sizeLimitAdmin'] : $config['cdn']['sizeLimitPublic']);
			$result = $uploader->handleUpload($config['cdn']['uploadDirectory']);
						
			$uploadStrategy = new $config['cdn']['upload_strategies'][$imageType];
			
			$uploadStrategy->setServiceLocator($this->getServiceLocator());
			$uploadStrategy->setUploadPath($config['cdn']['upload_paths'][$imageType]);	
			$uploadStrategy->setPublicPath($config['cdn']['public_paths'][$imageType]);	
			$uploadStrategy->addImage($imageType, $contentId, $userId, $isAdminRequest);
				
			if(isset($result['success'])) {
			
				$request = new Request($imageType, $imageType, null, null);			
				$image = Image::factory($request);

				if($image->moveImage($result['filePath'], $uploadStrategy->getDestinationPath()))
				{

					$uploadStrategy->imageReady();
					$uploadStrategy->removeCachedImages();
								
					$result = (object) array('success' => true, 'url' => $this->cdn($imageType, $uploadStrategy->getImageId()), 'id' => $uploadStrategy->getImageId());
				}
				else
				{
					$uploadStrategy->removeImage($result['filePath']);
					$result = (object) array('error'=>'Moving failed');
				}
			} else {
			
				$uploadStrategy->removeImage($result['filePath']);
				$result = (object) array('error'=>'Upload failed');
			}
			
			// to pass data through iframe you will need to encode all html tags
			echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
			
		} catch (\Exception $e) {
			
			die($e->getMessage());
		}

				
		exit;
	}
}