<?php

namespace Cdn\Exception;

class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}