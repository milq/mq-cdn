<?php


return array(
	'service_manager' => array(
    	'invokables' => array(
        	'HttpError' => 'Cdn\Service\HttpError',
		),
	),
    'controllers' => array(
        'invokables' => array(
            'Cdn\Controller\Index' => 'Cdn\Controller\IndexController',
            'Cdn\Controller\Docs' => 'Cdn\Controller\DocsController',
            'Cdn\Controller\Image' => 'Cdn\Controller\ImageController',
            'Cdn\Controller\Upload' => 'Cdn\Controller\UploadController',
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'cdn' => 'Cdn\Controller\Plugin\Cdn',
		),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'cdn' => array(
	            'type' => 'hostname',
            	'may_terminate' => true,
                'options' => array(
                    'route' => 'cdn.:domain',
					'defaults' => array(
			          	'__NAMESPACE__' => 'Cdn\Controller',
					  	'controller' 	=> 'Docs',
		                'action' 		=> 'index',
		            ),
                ),
                'child_routes' => array(
                	'documentation' => array(
	                    'type' => 'segment',
	                    'may_terminate' => true,
	                    'options' => array(
	                    	'route' => '/:docs',
	                       	'constraints' => array(
			             	   'docs' => 'docs|documentation',
			                ),
			                'defaults' => array(
			                	'__NAMESPACE__' => 'Cdn\Controller',
		                        'controller' 	=> 'Docs',
		                        'action' 		=> 'index',
		                    ),
	                     ),
					),
					'upload' => array(
	                    'type' => 'segment',
	                    'may_terminate' => true,
	                    'options' => array(
	                    	'route' => '/upload[/:source]',
	                    	'constraints' => array(
			             	   'source' => 'admin|user',
			                ),
			                'defaults' => array(
			                	'__NAMESPACE__' => 'Cdn\Controller',
		                        'controller' 	=> 'Upload',
		                        'action' 		=> 'index',
		                    ),
	                     ),
					),
					'image' => array(
	                    'type' => 'segment',
	                    'may_terminate' => false,
	                    'options' => array(
	                    	'route' => '[/:namespace]/:imageType/:dir/:imageId[-:params].jpg',
	                       	'constraints' => array(
			             	   'imageType' 	=> '[a-zA-Z0-9_-]*',
			             	   'dir' 		=> '[0-9]*',
			             	   'imageId' 	=> '[a-zA-Z0-9]*',
			                ),
			                'defaults' => array(
			                	'__NAMESPACE__' => 'Cdn\Controller',
		                        'controller' 	=> 'Image',
		                        'action' 		=> 'index',
		                    ),
	                     ),
					),
				),
            ),
        ),
    ),
    'view_helpers' => array(  
        'invokables' => array(    
            'cdn' => 'Cdn\View\Helper\Cdn',
        ),
    ),
    'view_manager' => array( 
        'strategies' => array(
            'ViewJsonStrategy',
        ),
        'default_suffix' => 'tpl', 
	    'display_not_found_reason' => true,
	    'display_exceptions'       => true,
	    'template_map' => array(
	        'layout/cdn' => __DIR__ . '/../view/layout/cdn.tpl',
	    ),
	    'template_path_stack' => array(
	        __DIR__ . '/../view',
	    ),
    ),
);