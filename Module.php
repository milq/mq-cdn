<?php
namespace Cdn;
 
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{	
	public function init(ModuleManager $moduleManager)
    {    		
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', function($e) {
			
			if(get_class($e->getTarget()) !== 'Application\Controller\AuthController') {
	
	            $controller = $e->getTarget();
				$controller->layout('layout/cdn');
			}
            
        }, 100); 
    }
    
    public function onBootstrap(MvcEvent $event)
    {   							
    	$app = $event->getApplication();    	
    	$eventManager   = $app->getEventManager();

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
  
        if(getenv('APPLICATION_MODULE') == 'cdn')
	        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'handleDispatchErrors'), 100); 
    }
    
    public function handleDispatchErrors($event) {
	    
	    $app = $event->getApplication();   
	    $serviceManager = $app->getServiceManager();
	    
	    $httpError = $serviceManager->get('HttpError');
	    $httpError->notFound();		
    }
    
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
        
    }
 
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}